/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

#include <GLES/gl.h>
#include "../../common/window.h"
#include "../../common/display.h"
#include "../../common/shader.h"
#include "../../common/texture.h"
#include "../../common/md2.h"



void gluPerspective(
	GLfloat fovy, GLfloat aspect,
	GLfloat zNear, GLfloat zFar)
{
	GLfloat f = 1.0f / tanf((fovy * M_PI) / 360.0f);
	GLfloat m[16] =
	{
		(f / aspect), 0.0f, 0.0f, 0.0f,
		0.0f, f, 0.0f, 0.0f,
		0.0f, 0.0f,
		((zFar + zNear) / (zNear - zFar)),
		((2.0f * zFar * zNear) / (zNear - zFar)),
		0.0f, 0.0f, -1.0f, 0.0f
	};
	glMultMatrixf(m);
}



void draw(md2_model_c* model)
{
	float t
		= (clock() * 12.0f)
			/ CLOCKS_PER_SEC;

	glLoadIdentity();
	glTranslatef(0.0, 0.0, -7.0);
	glRotatef(-90.0, 0.0, 1.0, 0.0);
	glRotatef(-90.0, 1.0, 0.0, 0.0);
	glScalef(0.125f, 0.125f, 0.125f);

	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, (GLfloat[4]){1.0, 0.5, 1.0, 1.0});

	md2_model_draw_tween(model, t);
}



int main(int argc, char* argv[])
{
	const char* _texture_path;
	const char* _model_path;

	#if defined(__ANDROID__)
	_texture_path = "/sdcard/texture.tga";
	_model_path   = "/sdcard/ogre.md2";
	#else
	_texture_path = "../texture.tga";
	_model_path   = "../ogre.md2";
	#endif

	if (!display_init(
		"Hello lighting - GL ES 1.x",
		320, 240,
		display_renderer_gles1x))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}

	/* Load texture. */
	glActiveTexture(GL_TEXTURE0);
	GLuint texture;
	{
		texture_t* tex
			= texture_import_tga(_texture_path);
		if (!tex)
		{
			fprintf(stderr, "Error: Unable to import texture.\n");
			return EXIT_FAILURE;
		}
		texture = texture_upload_es1x(tex);
		texture_delete(tex);
	}

	/* Load model. */
	FILE* fp = fopen(_model_path, "r");
	if (!fp)
	{
		fprintf(stderr, "Error: "
			"Unable to open model file '%s'.\n",
			_model_path);
		return EXIT_FAILURE;
	}
	md2_model_c* model
		= md2_model_import(fp);
	fclose(fp);
	if (!model)
	{
		fprintf(stderr, "Error: Unable to import model.\n");
		return EXIT_FAILURE;
	}
	
	/* GL Initialization */
	glViewport(0, 0,
		native_window_width(),
		native_window_height());
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepthf(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f,
		(GLfloat)native_window_width()
			/ (GLfloat)native_window_height(),
		1.0f, 1024.0f);
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);

	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw(model);

		display_swap();
		sleep(0); /* Give up remainder of time-slice. */
	}

	return EXIT_SUCCESS;
}
