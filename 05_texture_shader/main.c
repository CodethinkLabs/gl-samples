/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include <GLES2/gl2.h>
#include "../common/window.h"
#include "../common/display.h"
#include "../common/shader.h"
#include "../common/texture.h"



GLuint link_shaders(GLuint vert_shader, GLuint frag_shader)
{
	GLuint program = glCreateProgram();
	if (program == 0)
		return 0;

	glAttachShader(program, vert_shader);
	glAttachShader(program, frag_shader);

	glBindAttribLocation(program, 0, "aPosition");
	glBindAttribLocation(program, 1, "aTexCoord");

	glLinkProgram(program);

	GLint linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		GLint len = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
		if (len > 1)
		{
			char* log = malloc(len * sizeof(char));
			glGetProgramInfoLog(program, len, NULL, log);
			fprintf(stderr, "Error: Failed to link shader:\n%.*s\n", len, log);
			free(log);
		}

		glDeleteProgram(program);
		return 0;
	}

	return program;
}

GLuint load(const char* vertex, const char* fragment)
{
	const char* vext
		= strstr(vertex, ".glsl");
	bool vsrc = (vext && (vext[5] == '\0'));

	GLuint binary_format = 0;
	bool  has_binary_format = false;
	{
		GLint count;
		glGetIntegerv(
			GL_NUM_SHADER_BINARY_FORMATS,
			&count);
		has_binary_format
			= (count > 0);
		if (has_binary_format)
		{
			GLint binary_formats[count];
			glGetIntegerv(GL_SHADER_BINARY_FORMATS,
				binary_formats);
			binary_format = binary_formats[0];
		}
	}

	GLuint vert_shader;
	if (vsrc)
	{
		vert_shader = shader_compile_file(
			GL_VERTEX_SHADER, vertex);
	} else {
		if (!has_binary_format)
		{
			fprintf(stderr, "Error: No binary formats supported.\n");
			return 0;
		}
		vert_shader = shader_import_file(
			GL_VERTEX_SHADER, binary_format, vertex);
	}

	const char* fext
		= strstr(fragment, ".glsl");
	bool fsrc = (fext && (fext[5] == '\0'));

	GLuint frag_shader;
	if (fsrc)
	{
		frag_shader = shader_compile_file(
			GL_FRAGMENT_SHADER, fragment);
	} else {
		if (!has_binary_format)
		{
			fprintf(stderr, "Error: No binary formats supported.\n");
			return 0;
		}
		frag_shader = shader_import_file(
			GL_FRAGMENT_SHADER, binary_format, fragment);
	}

	GLuint program
		= link_shaders(vert_shader, frag_shader);
	return program;
}

void draw(GLuint program)
{
	static GLfloat verts[] =
	{
		 0.0f,  0.5f, 0.0f, 0.5f, 0.0f,
		-0.5f, -0.5f, 0.0f, 0.0f, 1.0f,
		 0.5f, -0.5f, 0.0f, 1.0f, 1.0f,
	};

	glUseProgram(program);

	glVertexAttribPointer(
		0, 3, GL_FLOAT, GL_FALSE,
		(5 * sizeof(GLfloat)), &verts[0]);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(
		1, 2, GL_FLOAT, GL_FALSE,
		(5 * sizeof(GLfloat)), &verts[3]);
	glEnableVertexAttribArray(1);

	glDrawArrays(GL_TRIANGLES, 0, 3);
}



int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		fprintf(stderr,
			"Error: Must specify vertex and fragment shaders.\n");
		return EXIT_FAILURE;
	}

	if (!display_init(
		"Hello Binary Shader - GL ES 2.0",
		320, 240,
		display_renderer_gles20))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}
	
	GLuint program = load(argv[1], argv[2]);
	if (program == 0)
	{
		fprintf(stderr, "Error: Failed to load/link shader program.\n");
		return EXIT_FAILURE;
	}

	glActiveTexture(GL_TEXTURE0);
	GLuint texture;
	{
		texture_t* tex
			= texture_import_tga(
			#if defined (__ANDROID__)
				"/sdcard/"
			#endif
				"texture.tga");
		if (!tex)
		{
			fprintf(stderr, "Error: Unable to import texture.\n");
			return EXIT_FAILURE;
		}
		texture = texture_upload_es20(tex);
		texture_delete(tex);
	}
	glUniform1i(glGetUniformLocation(program, "uTexture"), 0);

	/* GL Initialization */
	glViewport(0, 0,
		native_window_width(),
		native_window_height());
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT);

		draw(program);

		display_swap();

		sleep(0);  // Give up remainder of time-slice.
	}

	return EXIT_SUCCESS;
}
