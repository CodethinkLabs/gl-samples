precision mediump float;

uniform sampler2D uTexture;
varying vec2      vTexCoord;

^sampler = varying[0].xy,
^texture = sampler2D(0),
$0 = ^texture,
sync, stop;
