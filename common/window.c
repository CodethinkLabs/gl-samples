/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

static unsigned _window_width  = 0;
static unsigned _window_height = 0;

#if defined(__ANDROID__)
#include <android/native_window.h>

extern ANativeWindow* android_createDisplaySurface();

static ANativeWindow* _window = NULL;

bool native_window(
	const char* title,
	unsigned x, unsigned y,
	unsigned w, unsigned h,
	int visual_id, ANativeWindow** window)
{
	if (_window)
	{
		fprintf(stderr, "Error: Window already open.\n");
		return false;
	}

	_window = android_createDisplaySurface();

	_window_width  = ANativeWindow_getWidth(_window);
	_window_height = ANativeWindow_getHeight(_window);
	if (window)
		*window = _window;
	return true;
}

#else

#include <X11/Xlib.h>
#include <X11/Xutil.h>



static Window   _window;
static Display* _display;

static void _native_window_term()
{
	if (!_display)
		return;
	XCloseDisplay(_display);
}

static Bool _native_window_on_map(
	Display* d, XEvent* e, Window* w)
{
	return (e->type == MapNotify)
			&& (e->xmap.window == *w);
}

bool native_window(
	const char* title,
	unsigned x, unsigned y,
	unsigned w, unsigned h,
	int visual_id, Window* window)
{
	if (!window)
	{
		fprintf(stderr, "Error: No output specified.\n");
		return false;
	}
	if (_display)
	{
		fprintf(stderr, "Error: Window already open.\n");
		return false;
	}

	_display = XOpenDisplay(NULL);
	if (!_display)
	{
		fprintf(stderr, "Error: Failed to open X11 display.\n");
		return false;
	}

	static bool _atexit = false;
	if (!_atexit)
	{
		atexit(_native_window_term);
		_atexit = true;
	}

	Window root = RootWindow(_display,
		DefaultScreen(_display));

	int count;
	XVisualInfo template = { .visualid = visual_id, };
	XVisualInfo* visual_info
		= XGetVisualInfo(
			_display, VisualIDMask,
			&template, &count);
	if (!visual_info
		|| (count == 0))
	{
		fprintf(stderr, "Error: Failed to find matching X11 visual info.\n");
		return false;
	}

	static Visual _visual;
	_visual = *(visual_info[0].visual);
	unsigned visual_depth
		= visual_info[0].depth;
	XFree(visual_info);

	static Colormap _colormap;
	_colormap = XCreateColormap(
		_display, root,
		&_visual, AllocNone);

	XSetWindowAttributes window_attr =
	{
		.colormap         = _colormap,
		.background_pixel = 0xFFFFFFFF,
		.border_pixel     = 0x00000000,
		.event_mask       = StructureNotifyMask | ExposureMask,
	};

	Window _window
		= XCreateWindow(
		   _display, root,
		   0, 0, w, h, 0,
		   visual_depth, InputOutput, &_visual,
		   CWBackPixel | CWBorderPixel | CWEventMask | CWColormap,
		   &window_attr);

	XSizeHints size_hints =
	{
		.flags = USPosition,
		.x = 0,
		.y = 0,
	};
	XSetStandardProperties(
		_display, _window,
		title, title, None, 0, 0, &size_hints);
	XMapWindow(_display, _window);

	XEvent e;
	XIfEvent(_display, &e, (void*)_native_window_on_map, (char*)&_window);
	XSetWMColormapWindows(_display, _window, &_window, 1);
	XFlush(_display);

	_window_width  = w;
	_window_height = h;
	if (window)
		*window = _window;
	return true;
}

#endif



unsigned native_window_width()
{
	return _window_width;
}

unsigned native_window_height()
{
	return _window_height;
}
