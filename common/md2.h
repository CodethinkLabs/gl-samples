#ifndef __md2_h__
#define __md2_h__

#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif
	
typedef void md2_model_c;

extern void         md2_model_delete(md2_model_c* model);
extern md2_model_c* md2_model_import(FILE* stream);
extern void         md2_model_draw(md2_model_c* model, float frame);

/*
 * md2_model_draw_tween requires an attribute vertex array bound to index 2.
 */
extern void         md2_model_draw_tween(md2_model_c* model, float frame);

#ifdef __cplusplus
}
#endif
		
	
#endif
