/* Author(s):
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
 /* Light model, implements a lighting model with GLES1-like syntax */
#ifndef LIGHT_MODEL_H
#define LIGHT_MODEL_H

#include <GLES2/gl2.h>
#include <stdbool.h>

#define MAX_LIGHTS 16

enum lm_params {
	/* for lmEnable/lmDisable */
	LM_LIGHTING,

	/* LightModel params */
	LM_LIGHT_MODEL_TWO_SIDE,
	LM_LIGHT_MODEL_AMBIENT,

	/* Shared light and material parameters */
	LM_AMBIENT,
	LM_DIFFUSE,
	LM_SPECULAR,
	/* Allowed material faces */
	LM_FRONT_AND_BACK,

	/* Material parameters */
	LM_SHININESS,
	LM_EMISSION,
	LM_AMBIENT_AND_DIFFUSE,

	/* Light parameters */
	LM_POSITION,
	LM_SPOT_DIRECTION,
	LM_SPOT_EXPONENT,
	LM_SPOT_CUTOFF,
	LM_CONSTANT_ATTENUATION,
	LM_LINEAR_ATTENUATION,
	LM_QUADRATIC_ATTENUATION,

	/* Lights, past LIGHT9 can be done with LM_LIGHT0 + i */
	LM_LIGHT0,
	LM_LIGHT1,
	LM_LIGHT2,
	LM_LIGHT3,
	LM_LIGHT4,
	LM_LIGHT5,
	LM_LIGHT6,
	LM_LIGHT7,
	LM_LIGHT8,
	LM_LIGHT9,
};

struct lmlight {
	bool enabled;

	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
	GLfloat spot_direction[4];

	GLfloat spot_exponent;
	GLfloat spot_cutoff;
	GLfloat constant_attenuation;
	GLfloat linear_attenuation;
	GLfloat quadratic_attenuation;
	

};

struct lmmaterial {
	GLfloat emission[4];
	GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];

	GLfloat shininess;
};

struct lmstruct {
	bool enabled;

	GLint lightcount; /* Lightcount is the highest number of light that */
	                  /* is enabled */
	GLfloat ambient[4];
	bool twoside; /* if false backface is drawn using frontface's colour */
	              /* if true backface is drawn with reversed normals */
	struct lmmaterial material;
	struct lmlight *lights;
};

extern struct lmstruct lightmodel;

void lmEnable(GLint mode);

void lmDisable(GLint mode);

void lmMaterialfv(GLint face, GLint pname, GLfloat *params);

void lmLightModelfv(GLint pname, GLfloat *params);

void lmLightfv(GLint light, GLint pname, GLfloat *params);

void lmSend(GLint program);

#endif
