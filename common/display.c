/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <EGL/egl.h>

#include "window.h"
#include "display.h"

static EGLDisplay          egl_display = EGL_NO_DISPLAY;
static EGLConfig           egl_config;
static EGLContext          egl_context = EGL_NO_CONTEXT;
static EGLNativeWindowType egl_window;
static EGLSurface          egl_surface = EGL_NO_SURFACE;



static void display_term()
{
	eglTerminate(egl_display);
}

bool display_init(
	const char* title,
	unsigned width, unsigned height,
	display_renderer_e renderer)
{
	if (egl_display != EGL_NO_DISPLAY)
	{
		fprintf(stderr, "Error: Display already initialized.\n");
		return false;
	}

	EGLint config_attrib_list[] =
	{
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_DEPTH_SIZE, 16,
		EGL_NONE
	};
	EGLint context_attrib_list[] =
	{
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	switch (renderer)
	{
		case display_renderer_gles1x:
			config_attrib_list[1]  = EGL_OPENGL_ES_BIT;
			context_attrib_list[1] = 1;
			break;
		case display_renderer_gles20:
			break;
		default:
			fprintf(stderr, "Error: Unsupported renderer.\n");
			return false;
	}

	egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
	if (egl_display == EGL_NO_DISPLAY)
	{
		fprintf(stderr, "Error: Couldn't find display.\n");
		return false;
	}

	EGLint version[2];
	if (!eglInitialize(egl_display,
		&version[0], &version[1]))
	{
		fprintf(stderr, "Error: Failed to initialize.\n");
		return false;
	}
	printf("EGL v%d.%d initialized.\n",
		version[0], version[1]);

	static bool display_init_atexit = false;
	if (!display_init_atexit)
	{
		atexit(display_term);
		display_init_atexit = true;
	}

	EGLint config_count;
	if (!eglChooseConfig(
		egl_display,
		config_attrib_list,
		&egl_config, 1, &config_count))
	{
		fprintf(stderr, "Error: Failed to choose EGL config.\n");
		return false;
	}
	if (config_count == 0)
	{
		fprintf(stderr, "Error: No matching configurations found.\n");
		return false;
	}

	egl_context
		= eglCreateContext(
			egl_display, egl_config,
			EGL_NO_CONTEXT,
			context_attrib_list);
	if (egl_context == EGL_NO_CONTEXT)
	{
		fprintf(stderr, "Error: Unable to create EGL context.\n");
		return false;
	}

	EGLint visual_id;
	if (!eglGetConfigAttrib(
		egl_display, egl_config,
		EGL_NATIVE_VISUAL_ID, &visual_id))
	{
		fprintf(stderr, "Warning: Failed to get visual ID from EGL.\n");
		return false;
	}
	if (!native_window(
		title,
		0, 0, width, height,
		visual_id, &egl_window))
	{
		fprintf(stderr, "Error: Unable to open native window.\n");
		return false;
	}

	egl_surface
		= eglCreateWindowSurface(
			egl_display, egl_config,
			egl_window, NULL);
	if (egl_surface == EGL_NO_SURFACE)
	{
		fprintf(stderr, "Error: Unable to create EGL surface from native window.\n");
		return false;
	}
	
	if (!eglMakeCurrent(
		egl_display,
		egl_surface, egl_surface,
		egl_context))
	{
		fprintf(stderr, "Error: Unable to make EGL context current.\n");
		return false;
	}

	return true;
}



void display_swap()
{
	eglSwapBuffers(egl_display, egl_surface);
}
