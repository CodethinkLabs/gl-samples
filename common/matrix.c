/* Author(s):
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include "matrix.h"

#include <stdint.h>
#include <stdio.h>
void
matrixcopy(float *mout, const float *min)
{
	unsigned i;
	for(i = 0; i < 16; i++)
		mout[i] = min[i];
}

void
matrixmultiply(float *mat, const float *m1, const float *m2)
{
	if ((mat == m1) || (mat == m2)) {
		float temp[16];
		matrixmultiply(temp, m1, m2);
		matrixcopy(mat, temp);
		return;
	}
        uint8_t i, j, k;
        for(j = 0; j < 4; j++) {
                for(i = 0; i < 4; i++) {
                        mat[(j << 2) + i] = 0;
                        for(k = 0; k < 4; k++)
                                mat[(j << 2) + i] 
                                += (m1[(k << 2) + i] * m2[(j << 2) + k]);
                }
        }
}

void
matrixvecmultiply(float *vout, const float *min, float *vin)
{
	uint8_t i, j;
	if (vout == vin) {
		float temp[4];
		matrixvecmultiply(temp, min, vin);
		for(i = 0; i < 4; i++) {
			vout[i] = temp[i];
		}
		return;
	}
	for(i = 0; i < 4 ;i++) {
		vout[i] = 0;
		for(j = 0; j < 4; j++) {
			vout[i] += vin[j] * min[i + j * 4];
		}
	}
}

void
matrixidentity(float *mat)
{
	int i;
	for (i = 0; i < 16; i++) {
		switch (i) {
		case 0:
		case 5:
		case 10:
		case 15:
			mat[i] = 1;
			break;
		default:
			mat[i] = 0;
			break;
		}
	}
}

void
matrixprint(float *mat)
{
	uint8_t i;
	for (i = 0; i < 16; i++) {
		printf("%f, ", mat[i]);
		switch (i) {
		case 3:
		case 7:
		case 11:
		case 15:
			printf("\n");
			break;
		default:
			break;
		}
	}
}
