/* Author(s):
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/** Transform:
 * A library to emulate some of the matrix transformation functions that were
 * available in GLES1.x for GLES2.
 */

#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <GLES2/gl2.h>
 
#include "matrix.h"

enum tr_params {
	TR_MODEL,
	TR_VIEW,
	TR_PROJECTION,
	TR_MATRIX_MODE,
	
};

extern int tr_current_matrix_mode;
extern GLfloat *tr_current_matrix;
extern GLint* tr_current_matrix_loc_p;

extern GLfloat tr_model[16];
extern GLint tr_model_loc;
extern GLfloat tr_view[16];
extern GLint tr_view_loc;
extern GLfloat tr_projection[16];
extern GLint tr_projection_loc;

/** trMatrixMode:
 * Sets the current matrix mode.
 * Recognises the model, view and projection matrices.
 * Sets tr_current_matrix_mode, tr_current_matrix and tr_current_matrix_loc_p
 */
void trMatrixMode (int mode);

/** trGet:
 * Used to get data, like glGet.
 * If mode is TR_MATRIX_MODE, it returns TR_MODEL, TR_VIEW or TR_PROJECTION 
 * in params.
 * If mode is TR_MODEL, it returns a pointer to the model matrix as an array.
 * If mode is TR_VIEW, it returns a pointer to the view matrix as an array.
 * If mode is TR_PROJECTION, it returns a pointer to the projection matrix as
 * an array.
 */
void trGetIntegerv(int mode, GLint *params);
void trGetFloatv(int mode, GLfloat *params);

/** trBind:
 * Makes the uniforms available for the shader.
 */
void trBind(GLint program, int matrixsymbol, const char *name);

/** trSend:
 * Sends the current matrix to the shader.
 */
void trSend();

/** trLoadIdentity:
 * Resets the current matrix to the Identity matrix.
 */
void trLoadIdentity();

/** trLoadMatrix:
 * Copies the contents of m to the current matrix.
 */
void trLoadMatrix(const GLfloat *m);

/** trScale:
 * Multiplies the current matrix by a scaling matrix.
 * Note: It does not prevent the user from scaling matrices that aren't model
 * or view.
 */
void trScale(GLfloat x, GLfloat y, GLfloat z);
/** trRotate:
 * Multiplies the current matrix by a rotation matrix.
 * Note: It does not prevent the user from rotating matrices that aren't model
 * or view.
 */
void trRotate(GLfloat angle, GLfloat inx, GLfloat iny, GLfloat inz);

/** trTranslate:
 * Multiplies the current matrix by a translation matrix.
 * Note: It does not prevent the user from rotating matrices that aren't model
 * or view.
 */
void trTranslate(GLfloat x, GLfloat y, GLfloat z);
/** trFrustum:
 * Multiplies the current matrix by a frustum matrix.
 * Note: It does not prevent the user from rotating matrices that aren't 
 * perspective.
 */
void trFrustum(GLfloat l, GLfloat r, GLfloat t, GLfloat b, GLfloat n, GLfloat f);
/** trPerspective:
 * Calculates the appropriate arguments for and calls trFrustum to perform a 
 * perspective transform on the current matrix.
 */
void trPerspective(GLfloat near, GLfloat far, GLfloat width, GLfloat height, GLfloat angle);


#endif
