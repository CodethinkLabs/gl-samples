#include <stdlib.h>
#include <stdint.h>
#include <GLES/gl.h>



/* TODO - Not duplicated these structs. */
typedef struct
__attribute__((__packed__))
{
	float x, y, z;
	float nx, ny, nz;
	float u, v;
} _md2_model_vertex_t;

typedef struct
{
	uint32_t frames;
	uint32_t triangles;	
	_md2_model_vertex_t* verts;
} md2_model_c;



void md2_model_draw(md2_model_c* model, float frame)
{
	if(model == NULL)
		return;
	uint32_t iframe = (uint32_t)frame % model->frames;
	
	glVertexPointer(3, GL_FLOAT,
		sizeof(_md2_model_vertex_t),
		&model->verts[iframe * model->triangles * 3].x);
	glTexCoordPointer(2, GL_FLOAT,
		sizeof(_md2_model_vertex_t),
		&model->verts[iframe * model->triangles * 3].u);
	glNormalPointer(GL_FLOAT,
		sizeof(_md2_model_vertex_t),
		&model->verts[iframe * model->triangles * 3].nx);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	
	glDrawArrays(GL_TRIANGLES, 0, (model->triangles * 3));
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}

static inline float lerpf(float a, float b, float f)
{
	return ((b * f) + (a * (1.0f -f)));
}

void md2_model_draw_tween(md2_model_c* model, float frame)
{
	if (model == NULL)
		return;
	uint32_t iframe = (uint32_t)frame % model->frames;
	uint32_t inextframe = (iframe + 1) % model->frames;
	float tween = (frame - (unsigned)frame);
	
	unsigned vcount
		= (model->triangles * 3);
	_md2_model_vertex_t verts[vcount];

	unsigned i;
	_md2_model_vertex_t* a
		= &model->verts[iframe * vcount];
	_md2_model_vertex_t* b
		= &model->verts[inextframe * vcount];
	_md2_model_vertex_t* c = verts;	
	for (i = 0; i < vcount; i++, a++, b++, c++)
	{
		c->x = lerpf(a->x, b->x, tween);
		c->y = lerpf(a->y, b->y, tween);
		c->z = lerpf(a->z, b->z, tween);

		c->nx = lerpf(a->nx, b->nx, tween);
		c->ny = lerpf(a->ny, b->ny, tween);
		c->nz = lerpf(a->nz, b->nz, tween);

		c->u = lerpf(a->u, b->u, tween);
		c->v = lerpf(a->v, b->v, tween);
	}

	glVertexPointer(3, GL_FLOAT,
		sizeof(_md2_model_vertex_t),
		&verts[0].x);
	glTexCoordPointer(2, GL_FLOAT,
		sizeof(_md2_model_vertex_t),
		&verts[0].u);
	glNormalPointer(GL_FLOAT,
		sizeof(_md2_model_vertex_t),
		&verts[0].nx);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	
	glDrawArrays(GL_TRIANGLES, 0, (model->triangles * 3));
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
}
