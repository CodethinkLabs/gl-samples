/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include "texture.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>



typedef enum
{
	tga_image_type_none  = 0,
	tga_image_type_index = 1,
	tga_image_type_color = 2,
	tga_image_type_gray  = 3,
} tga_image_type_e;

typedef struct
__attribute__((__packed__))
{
	unsigned         ident_size  :  8;
	unsigned         clut_type   :  8;
	tga_image_type_e image_type  :  2;
	bool             rle_encoded :  1;
	unsigned         reserved_0  :  5;
	unsigned         clut_start  : 16;
	unsigned         clut_size   : 16;
	unsigned         clut_bpp    :  8;
	signed           origin_x    : 16;
	signed           origin_y    : 16;
	signed           width       : 16;
	signed           height      : 16;
	unsigned         bpp         :  8;
	unsigned         alpha       :  4;
	bool             h_flip      :  1;
	bool             v_flip      :  1;
	unsigned         reserved_1  :  2;
} tga_header_t;



texture_t* texture_import_tga(const char* path)
{
	FILE* fp = fopen(path, "rb");
	if (!fp) return NULL;

	tga_header_t header;
	if ((fread(&header, sizeof(header), 1, fp) != 1)
		|| (header.clut_type != 0)
		|| (header.reserved_0 != 0)
		|| header.rle_encoded
		|| (header.image_type != tga_image_type_color)
		|| (header.width <= 0)
		|| (header.width & (header.width - 1))
		|| (header.height <= 0)
		|| (header.height & (header.height - 1))
		|| (header.bpp != 32)
		|| (header.reserved_1 != 0)
		|| !header.v_flip
		|| header.h_flip
		|| (header.alpha != 8))
	{
		fclose(fp);
		return NULL;
	}

	void* data
		= malloc(header.width
			* header.height * 4);
	if (!data)
	{
		fclose(fp);
		return NULL;
	}

	if (fread(data,
		(header.width * header.height * 4),
		1, fp) != 1)
	{
		free(data);
		fclose(fp);
		return NULL;
	}
	fclose(fp);

	texture_t* texture
		= (texture_t*)malloc(sizeof(texture_t));
	if (!texture)
	{
		free(data);
		return NULL;
	}
	texture->data   = data;
	texture->width  = header.width;
	texture->height = header.height;

	/* Convert BGRA to RGBA */
	uint8_t* sdata = (uint8_t*)data;
	unsigned i, j;
	for (i = 0, j = 0;
		i < (header.width * header.height);
		i++, j += 4)
	{
		uint8_t swap = sdata[j + 0];
		sdata[j + 0] = sdata[j + 2];
		sdata[j + 2] = swap;
	}

	return texture;
}

bool texture_export_tga(texture_t* texture, const char* path)
{
	if (!texture)
		return false;

	FILE* fp = fopen(path, "wb");
	if (!fp) return false;

	tga_header_t header =
	{
		.ident_size  = 0,
		.clut_type   = 0,
		.reserved_0  = 0x00,
		.rle_encoded = false,
		.image_type  = tga_image_type_color,
		.clut_start  = 0,
		.clut_size   = 0,
		.clut_bpp    = 0,
		.origin_x    = 0,
		.origin_y    = 0,
		.width       = texture->width,
		.height      = texture->height,
		.bpp         = 32,
		.reserved_1  = 0x0,
		.v_flip      = true,
		.h_flip      = false,
		.alpha       = 8
	};

	bool ret;
	ret = (fwrite(&header, sizeof(header), 1, fp) != 1);
	ret = ret && (fwrite(texture->data,
		(texture->width * texture->height * 4),
		1, fp) != 1);
	fclose(fp);
	return ret;
}

void texture_delete(texture_t* texture)
{
	if (!texture)
		return;
	free(texture->data);
	free(texture);
}
