#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "md2_norms.h"



typedef struct
__attribute__((__packed__))
{
	float x, y, z;
	float nx, ny, nz;
	float u, v;
} _md2_model_vertex_t;

typedef struct
{
	uint32_t frames;
	uint32_t triangles;	
	_md2_model_vertex_t* verts;
} md2_model_c;



typedef struct
__attribute__((__packed__))
{
	char     ident[4];
	uint32_t version;
	int32_t  skinwidth, skinheight;
	uint32_t framesize;
	uint32_t num_skins, num_xyz, num_st,
		num_tris, num_glcmds, num_frames;
	uint32_t offset_skins, offset_st, offset_tris,
		offset_frames, offset_glcmds, offset_end;
} _md2_header_t;

typedef struct
__attribute__((__packed__))
{
	uint8_t x, y, z, n;
} _md2_vertex_t;

typedef struct
__attribute__((__packed__))
{
	uint16_t s, t;
} _md2_texcoord_t;

typedef struct
__attribute__((__packed__))
{
	float x, y, z;
} _md2_vect3f_t;

typedef struct
__attribute__((__packed__))
{
	uint16_t v[3];
	uint16_t t[3];
} _md2_triangle_t;

typedef struct
__attribute__((__packed__))
{
	_md2_vect3f_t scale, translate;
	char          name[16];
} _md2_frame_t;




void md2_model_delete(md2_model_c* model)
{
	if(model == NULL)
		return;
	free(model->verts);
	free(model);
}

static inline _md2_model_vertex_t _md2_vertex_to_float(_md2_vertex_t mvert, _md2_texcoord_t mtcoord, _md2_vect3f_t scale, _md2_vect3f_t translate, int32_t twidth, int32_t theight)
{
	_md2_model_vertex_t vert =
	{
		(((float)mvert.x * scale.x) + translate.x),
		(((float)mvert.y * scale.y) + translate.y),
		(((float)mvert.z * scale.z) + translate.z),
		0.0f, 0.0f, 0.0f, // TODO - Normals
		((float)mtcoord.s / (float)twidth), ((float)mtcoord.t / (float)theight)
	};
	md2_norm(mvert.n, &vert.nx);
	return vert;
}

md2_model_c* md2_model_import(FILE* stream)
{
	if(stream == NULL)
		return NULL;
	
	_md2_header_t header;
	if(fread(&header, sizeof(_md2_header_t), 1, stream) != 1)
		return NULL;
	
	if(header.version != 8)
		return NULL;
	if(strncmp(header.ident, "IDP2", 4)  != 0)
		return NULL;
	
	md2_model_c* model = (md2_model_c*)malloc(sizeof(md2_model_c*));
	if(model == NULL)
		return NULL;
	model->frames    = header.num_frames;
	model->triangles = header.num_tris;
	model->verts     = (_md2_model_vertex_t*)malloc(sizeof(_md2_model_vertex_t) * (model->frames * model->triangles * 3));
	if(model->verts == NULL)
	{
		md2_model_delete(model);
		return NULL;
	}
	
	_md2_texcoord_t texcoords[header.num_st];
	if((fseek(stream, header.offset_st, SEEK_SET) != 0)
		|| (fread(texcoords, sizeof(_md2_texcoord_t), header.num_st, stream) != header.num_st))
	{
		md2_model_delete(model);
		return NULL;
	}
	
	_md2_triangle_t tris[header.num_tris];
	if((fseek(stream, header.offset_tris, SEEK_SET) != 0)
		|| (fread(tris, sizeof(_md2_triangle_t), header.num_tris, stream) != header.num_tris))
	{
		md2_model_delete(model);
		return NULL;
	}
	
	if(fseek(stream, header.offset_frames, SEEK_SET) != 0)
	{
		md2_model_delete(model);
		return NULL;
	}
	uintptr_t f, v;
	for(f = 0, v = 0; f < header.num_frames; f++, v += (header.num_tris * 3))
	{
		_md2_frame_t  frame;
		_md2_vertex_t verts[header.num_xyz];
		if((fread(&frame, sizeof(_md2_frame_t), 1, stream) != 1)
			|| (fread(verts, sizeof(_md2_vertex_t), header.num_xyz, stream) != header.num_xyz))
		{
			md2_model_delete(model);
			return NULL;
		}
		
		uintptr_t i;
		for(i = 0; i < header.num_tris; i++)
		{
			model->verts[v + (i * 3) + 0] = _md2_vertex_to_float(verts[tris[i].v[0]], texcoords[tris[i].t[0]], frame.scale, frame.translate, header.skinwidth, header.skinheight);
			model->verts[v + (i * 3) + 1] = _md2_vertex_to_float(verts[tris[i].v[1]], texcoords[tris[i].t[1]], frame.scale, frame.translate, header.skinwidth, header.skinheight);
			model->verts[v + (i * 3) + 2] = _md2_vertex_to_float(verts[tris[i].v[2]], texcoords[tris[i].t[2]], frame.scale, frame.translate, header.skinwidth, header.skinheight);
		}
	}
	
	return model;
}
