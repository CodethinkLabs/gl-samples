/* Author(s):
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "../lightmodel.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

struct lmstruct lightmodel = {
	.enabled = false,
	.material = {0},
	.lightcount = 0,
	.ambient = {0},
	.lights = NULL,
};

static inline void reallocate_lights(unsigned num)
{
	unsigned i;
	/* Reallocate data to ensure there is space */
	struct lmlight *new = realloc(lightmodel.lights,
		sizeof(*lightmodel.lights) * num);
	if (new == NULL) {
		fprintf(stderr, "Error: could not allocate memory for new "
			"light.\n");
		return;
	}
	/* Check whether reallocation was necessary */
	if (new == lightmodel.lights) {
	} else {
		lightmodel.lights = new;
		/* Zero out new data */
		for (i = lightmodel.lightcount; i < num; i++) {
			memset((lightmodel.lights + i), 0, sizeof(*lightmodel.lights));
		}
	}

}

void
lmEnable(GLint mode)
{
	if (mode == LM_LIGHTING) {
		lightmodel.enabled = true;
		
	} else if (mode >= LM_LIGHT0 && mode < LM_LIGHT0 + MAX_LIGHTS) {
		unsigned lightnum = mode - LM_LIGHT0;
		
		/* If that light will not have memory assigned */
		if (lightnum + 1 > lightmodel.lightcount) {
			reallocate_lights(lightnum + 1);
		}
		
		/* Enable the light */
		lightmodel.lights[lightnum].enabled = true;
		
		/* Set the new lightcount */
		if (lightmodel.lightcount < lightnum + 1)
			lightmodel.lightcount = lightnum + 1;
	
	} else {
		fprintf(stderr, "Error: Unrecognised mode.\n");
		if (mode >= LM_LIGHT0)
			fprintf(stderr, "Note: a maximum of %d lights are"
			" allowed.\n", MAX_LIGHTS);
	}
}

void
lmDisable(GLint mode)
{
	uint8_t i;
	if (mode == LM_LIGHTING) {
		lightmodel.enabled = false;
		
	} else if (mode >= LM_LIGHT0 && mode < LM_LIGHT0 + MAX_LIGHTS) {
		unsigned lightnum = mode - LM_LIGHT0;
		unsigned newlightnum;
		/* Mark the light as disabled */
		lightmodel.lights[lightnum].enabled = false;
		
		/* Find the highest number for enabled lights */
		for (i = 0, newlightnum = 0; i < lightmodel.lightcount; i++) {
			if (lightmodel.lights[i].enabled == true)
				newlightnum = i;
		}
		lightmodel.lightcount = newlightnum + 1;
		
	} else {
		fprintf(stderr, "Error: Unrecognised mode.\n");
		if (mode >= LM_LIGHT0)
			fprintf(stderr, "Note: a maximum of %d lights are"
			" allowed.\n", MAX_LIGHTS);
	}
}

void
lmMaterialfv(GLint face, GLint pname, GLfloat *params)
{
	uint8_t i;
	if (face != LM_FRONT_AND_BACK) {
		fprintf(stderr, "Error: In lmMaterialfv, the only argument "
		        "permitted for face is LM_FRONT_AND_BACK.\n");
		return;
	}
	switch (pname) {
	case LM_SHININESS:
		lightmodel.material.shininess = *params;
		break;
		
	case LM_EMISSION:
		for (i = 0; i < 4; i++) {
			lightmodel.material.emission[i] = params[i];
		}
		break;
		
	case LM_AMBIENT:
		for (i = 0; i < 4; i++) {
			lightmodel.material.ambient[i] = params[i];
		}
		break;
		
	case LM_DIFFUSE:
		for (i = 0; i < 4; i++) {
			lightmodel.material.diffuse[i] = params[i];
		}
		break;
		
	case LM_SPECULAR:
		for (i = 0; i < 4; i++) {
			lightmodel.material.specular[i] = params[i];
		}
		break;
		
	case LM_AMBIENT_AND_DIFFUSE:
		for (i = 0; i < 4; i++) {
			lightmodel.material.ambient[i] = params[i];
			lightmodel.material.diffuse[i] = params[i];
		}
		break;
		
	default:
		fprintf(stderr, "Error: Unrecognised parameter name to "
		        "lmMaterialfv.\n");
		break;
	}
}

void
lmLightModelfv(GLint pname, GLfloat *params)
{
	uint8_t i;
	switch (pname) {
	case LM_LIGHT_MODEL_TWO_SIDE:
		lightmodel.twoside = (*params != 0) ? true : false;
		break;
		
	case LM_LIGHT_MODEL_AMBIENT:
		for (i = 0; i < 4; i++) {
			lightmodel.ambient[i] = params[i];
		}
		break;
		
	default:
		fprintf(stderr, "Error: unrecognised parameter name to "
		        "lmLightModelfv.\n");
		break;
	}
}

void
lmLightfv(GLint light, GLint pname, GLfloat *params)
{
	uint8_t i;
	unsigned lightnum = light - LM_LIGHT0;
	if (lightnum < 0 || lightnum >= MAX_LIGHTS) {
		fprintf(stderr, "Error: Unrecognised light number to "
			"lmLightfv.\n");
		return;
	}
	/*
	 * Ensure there is memory available for this light
	 * Note: Causes worse performance if the light isn't enabled because
	 * it checks if you need more memory every time this function is 
	 * called.
	 */
	if (lightnum + 1 > lightmodel.lightcount) {
		reallocate_lights(lightnum + 1);
	}

	switch(pname) {
	case LM_AMBIENT:
		for (i = 0; i < 4; i++) {
			lightmodel.lights[lightnum].ambient[i] = params[i];
		}
		break;
		
	case LM_DIFFUSE:
		for (i = 0; i < 4; i++) {
			lightmodel.lights[lightnum].diffuse[i] = params[i];
		}
		break;
		
	case LM_SPECULAR:
		for (i = 0; i < 4; i++) {
			lightmodel.lights[lightnum].specular[i] = params[i];
		}
		break;
		
	case LM_POSITION:
		for (i = 0; i < 4; i++) {
			lightmodel.lights[lightnum].position[i] = params[i];
		}
		break;
		
	case LM_SPOT_DIRECTION:
		for (i = 0; i < 4; i++) {
			lightmodel.lights[lightnum].ambient[i] = params[i];
		}
		break;
		
	case LM_SPOT_EXPONENT:
		lightmodel.lights[lightnum].spot_exponent = *params;
		break;
		
	case LM_SPOT_CUTOFF:
		lightmodel.lights[lightnum].spot_cutoff = *params;
		break;
		
	case LM_CONSTANT_ATTENUATION:
		lightmodel.lights[lightnum].constant_attenuation = *params;
		break;
		
	case LM_LINEAR_ATTENUATION:
		lightmodel.lights[lightnum].linear_attenuation = *params;
		break;
		
	case LM_QUADRATIC_ATTENUATION:
		lightmodel.lights[lightnum].quadratic_attenuation = *params;
		break;
		
	default:
		fprintf(stderr, "Error: Unrecognised parameter name to "
			"lmLightfv.\n");
		break;
	}
}
static inline void 
write_name(const char *format, unsigned index, char *buffer)
{
	if (sprintf(buffer, format, index) < 0) {
		fprintf(stderr, "Error occurred writing name to buffer.\n");
		return;
	}
}
void
lmSend(GLint program)
{
	GLint loc;
	unsigned i;
	if (lightmodel.enabled == true) {
		loc = glGetUniformLocation(program, 
			"lightmodel.enabled");
		glUniform1i(loc, lightmodel.enabled);
		
		loc = glGetUniformLocation(program, 
			"lightmodel.twoside");
		glUniform1i(loc, lightmodel.twoside);
		
		loc = glGetUniformLocation(program, 
			"lightmodel.ambient");
		glUniform4fv(loc, 1, lightmodel.ambient);
		
		loc = glGetUniformLocation(program, 
			"lightmodel.lightcount");
		glUniform1i(loc, lightmodel.lightcount);
		
		
		loc = glGetUniformLocation(program, 
			"lightmodel.material.shininess");
		glUniform1f(loc, lightmodel.material.shininess);
		
		loc = glGetUniformLocation(program, 
			"lightmodel.material.emission");
		glUniform4fv(loc, 1, lightmodel.material.emission);

		loc = glGetUniformLocation(program, 
			"lightmodel.material.ambient");
		glUniform4fv(loc, 1, lightmodel.material.ambient);

		loc = glGetUniformLocation(program, 
			"lightmodel.material.diffuse");
		glUniform4fv(loc, 1, lightmodel.material.diffuse);
		
		loc = glGetUniformLocation(program, 
			"lightmodel.material.specular");
		glUniform4fv(loc, 1, lightmodel.material.specular);
		
		
		/* For each light*/
		for (i = 0; i < lightmodel.lightcount; i++) {
			char buffer[BUFSIZ];
			/* Send enabled */
			write_name("lightmodel.lights[%d].enabled", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform1i(loc, lightmodel.lights[i].enabled);
			/* If not enabled, stop sending for this light */
			if (lightmodel.lights[i].enabled == false)
				continue;
			
			write_name("lightmodel.lights[%d].ambient", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform4fv(loc, 1, lightmodel.lights[i].ambient);
			
			write_name("lightmodel.lights[%d].diffuse", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform4fv(loc, 1, lightmodel.lights[i].diffuse);

			write_name("lightmodel.lights[%d].specular", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform4fv(loc, 1, lightmodel.lights[i].specular);

			write_name("lightmodel.lights[%d].position", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform4fv(loc, 1, lightmodel.lights[i].position);

			write_name("lightmodel.lights[%d].spot_direction", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform4fv(loc, 1, lightmodel.lights[i].spot_direction);
			
			write_name("lightmodel.lights[%d].spot_exponent", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform1f(loc, lightmodel.lights[i].spot_exponent);
			
			write_name("lightmodel.lights[%d].spot_cutoff", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform1f(loc, lightmodel.lights[i].spot_cutoff);

			write_name("lightmodel.lights[%d].constant_attenuation", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform1f(loc, lightmodel.lights[i].constant_attenuation);

			write_name("lightmodel.lights[%d].linear_attenuation", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform1f(loc, lightmodel.lights[i].linear_attenuation);

			write_name("lightmodel.lights[%d].quadratic_attenuation", i, buffer);
			loc = glGetUniformLocation(program, buffer);
			glUniform1f(loc, lightmodel.lights[i].quadratic_attenuation);

		}
	}
}
