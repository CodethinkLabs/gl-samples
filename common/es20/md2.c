#include <stdlib.h>
#include <stdint.h>
#include <GLES2/gl2.h>



/* TODO - Not duplicated these structs. */
typedef struct
__attribute__((__packed__))
{
	float x, y, z;
	float nx, ny, nz;
	float u, v;
} _md2_model_vertex_t;

typedef struct
{
	uint32_t frames;
	uint32_t triangles;	
	_md2_model_vertex_t* verts;
} md2_model_c;



void md2_model_draw(md2_model_c* model, float frame)
{
	if(model == NULL)
		return;
	uint32_t iframe = (uint32_t)frame % model->frames;
		
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(_md2_model_vertex_t), 
		&model->verts[iframe * model->triangles * 3].x);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(_md2_model_vertex_t), 
		&model->verts[iframe * model->triangles * 3].u);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	glDrawArrays(GL_TRIANGLES, 0, (model->triangles * 3));
	
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void md2_model_draw_tween(md2_model_c* model, float frame)
{
	if (model == NULL)
		return;
	uint32_t iframe = (uint32_t)frame % model->frames;
	uint32_t inextframe = (iframe + 1) % model->frames;
	float tweenamount = (frame - (unsigned)frame);
	
	
	GLint program = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &program);
	float loc = glGetUniformLocation(program, "uTween");
	
	glUniform1f(loc, tweenamount);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(_md2_model_vertex_t), 
		&model->verts[iframe * model->triangles * 3].x);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(_md2_model_vertex_t), 
		&model->verts[iframe * model->triangles * 3].u);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE,
		sizeof(_md2_model_vertex_t),
		&model->verts[inextframe * model->triangles * 3].x);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	
	glDrawArrays(GL_TRIANGLES, 0, (model->triangles * 3));
	
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}
