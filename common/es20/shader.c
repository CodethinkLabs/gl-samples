/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include "../shader.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>



GLuint shader_compile(GLenum type,
	const char* source)
{
	GLuint shader;
	shader = glCreateShader(type);
	if (shader == 0)
		return 0;

	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	if (!compiled) 
	{
		GLint len = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
		if (len > 1)
		{
			char* log = malloc(len * sizeof(char));
			glGetShaderInfoLog(shader, len, NULL, log);
			fprintf(stderr, "Error: Failed to compile shader:\n%.*s\n", len, log);
			free(log);
		}

		glDeleteShader(shader);
		return 0;
	}

	return shader;
}

GLuint shader_import(GLenum type,
	GLenum format, const void* binary, GLsizei size)
{
	GLuint shader = glCreateShader(type);
	glShaderBinary(1, &shader,
		format, binary, size);
	return shader;
}



GLuint shader_compile_file(GLenum type,
	const char* path)
{
	FILE* fp = fopen(path, "rb");
	if (!fp) return 0;

	if (fseek(fp, 0, SEEK_END) < 0)
	{
		fclose(fp);
		return 0;
	}
	long len = ftell(fp);
	if ((len <= 0)
		|| (fseek(fp, 0, SEEK_SET) < 0))
	{
		fclose(fp);
		return 0;
	}

	char source[len + 1];
	bool was_read = (fread(source, len, 1, fp) == 1);
	fclose(fp);
	if (!was_read)
		return 0;
	source[len] = '\0';
	return shader_compile(type, source);
}

GLuint shader_import_file(GLenum type,
	GLenum format, const char* path)
{
	FILE* fp = fopen(path, "rb");
	if (!fp) return 0;

	if (fseek(fp, 0, SEEK_END) < 0)
	{
		fclose(fp);
		return 0;
	}
	long len = ftell(fp);
	if ((len <= 0)
		|| (fseek(fp, 0, SEEK_SET) < 0))
	{
		fclose(fp);
		return 0;
	}

	char source[len];
	bool was_read = (fread(source, len, 1, fp) == 1);
	fclose(fp);
	if (!was_read)
		return 0;
	return shader_import(type, format, source, len);
}
