/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

#include <GLES/gl.h>
#include "../common/window.h"
#include "../common/display.h"
#include "../common/shader.h"
#include "../common/texture.h"
#include "../common/transform.h"



void gluPerspective(
	GLfloat fovy, GLfloat aspect,
	GLfloat zNear, GLfloat zFar)
{
	GLfloat f = 1.0f / tanf((fovy * M_PI) / 360.0f);
	GLfloat m[16] =
	{
		(f / aspect), 0.0f, 0.0f, 0.0f,
		0.0f, f, 0.0f, 0.0f,
		0.0f, 0.0f,
		((zFar + zNear) / (zNear - zFar)),
		((2.0f * zFar * zNear) / (zNear - zFar)),
		0.0f, 0.0f, -1.0f, 0.0f
	};
	glMultMatrixf(m);
}



void draw()
{
	static GLfloat verts[] =
	{
		-1.0,  1.0,  1.0,
		 1.0,  1.0,  1.0,
		-1.0, -1.0,  1.0,
		 
		-1.0, -1.0,  1.0,
		 1.0, -1.0,  1.0,
		 1.0,  1.0,  1.0,
		
		-1.0,  1.0, -1.0,
		-1.0,  1.0,  1.0,
		-1.0, -1.0, -1.0,
		
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  1.0,  1.0,
	};
	float t
		= (clock() * 12.0f)
			/ CLOCKS_PER_SEC;
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glScalef(0.75, 0.75, 0.75);
	glTranslatef(0.0, 0.0, -3.0);
	glTranslatef(0.0, 0.0, sinf(t/20.0)/2.0);
	glRotatef(t*2, 0.0, 1.0, 0.0);
	
	glVertexPointer(3, GL_FLOAT, 0, verts);
	
	glColor4f(1.0, 0.0, 0.0, 1.0);
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glDrawArrays(GL_TRIANGLES, 0, 12);
	glDisableClientState(GL_VERTEX_ARRAY);
	
}



int main(int argc, char* argv[])
{

	if (!display_init(
		"Hello model - GL ES 1.x",
		320, 240,
		display_renderer_gles1x))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}
	
	/* GL Initialization */
	glViewport(0, 0,
		native_window_width(),
		native_window_height());
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepthf(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	gluPerspective(45.0f,
		(GLfloat)native_window_width()
			/ (GLfloat)native_window_height(),
		1.0f, 1024.0f);
	glMatrixMode(GL_MODELVIEW);

	glEnable(GL_TEXTURE_2D);
	
	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw();

		display_swap();
		sleep(0); /* Give up remainder of time-slice. */
	}

	return EXIT_SUCCESS;
}
