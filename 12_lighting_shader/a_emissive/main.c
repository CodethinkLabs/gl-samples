/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

#include <GLES2/gl2.h>
#include "../../common/window.h"
#include "../../common/display.h"
#include "../../common/shader.h"
#include "../../common/texture.h"
#include "../../common/transform.h"
#include "../../common/md2.h"
#include "../../common/lightmodel.h"


GLuint link_shaders(const char* vert_path, const char* frag_path)
{
	GLuint program = glCreateProgram();
	if (program == 0)
		return 0;

	GLuint vert_shader = shader_compile_file(GL_VERTEX_SHADER, vert_path);
	glAttachShader(program, vert_shader);

	GLuint frag_shader = shader_compile_file(GL_FRAGMENT_SHADER, frag_path);
	glAttachShader(program, frag_shader);

	glBindAttribLocation(program, 0, "aPosition");
	glBindAttribLocation(program, 1, "aTexCoord");
	glBindAttribLocation(program, 2, "aNextPos");

	glLinkProgram(program);

	GLint linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		GLint len = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
		if (len > 1)
		{
			char* log = malloc(len * sizeof(char));
			glGetProgramInfoLog(program, len, NULL, log);
			fprintf(stderr, "Error: Failed to link shader:\n%.*s\n", len, log);
			free(log);
		}

		glDeleteProgram(program);
		return 0;
	}

	return program;
}

void draw(GLuint program, md2_model_c* model)
{
	float t
		= (clock() * 12.0f)
			/ CLOCKS_PER_SEC;
	
	glUseProgram(program);

	trMatrixMode(TR_MODEL);
	trLoadIdentity();
	trScale(0.125, 0.125, 0.125);
	trSend();

	trMatrixMode(TR_VIEW);
	trLoadIdentity();
	trTranslate(0.0, 0.0, -7.0);
	trRotate(-90.0, 0.0, 1.0, 0.0);
	trRotate(90.0, 1.0, 0.0, 0.0);
	trSend();
	
	trMatrixMode(TR_PROJECTION);
	trLoadIdentity();
	trPerspective(
		1.0, 1024.0,
		native_window_width(),
		native_window_height(),
		45.0);
	trSend();
	
	lmMaterialfv(LM_FRONT_AND_BACK, LM_EMISSION, (float[4]){1.0, 0.5, 1.0, 1.0});
	lmSend(program);
	
	md2_model_draw_tween(model, t);
}



int main(int argc, char* argv[])
{
	const char* _vert_path;
	const char* _frag_path;
	const char* _texture_path;
	const char* _model_path;

	#if defined(__ANDROID__)
	_vert_path    = "/sdcard/vertex.glsl";
	_frag_path    = "/sdcard/fragment.glsl";
	_texture_path = "/sdcard/texture.tga";
	_model_path   = "/sdcard/ogre.md2";
	#else
	_vert_path    = "../vertex.glsl";
	_frag_path    = "../fragment.glsl";
	_texture_path = "../texture.tga";
	_model_path   = "../ogre.md2";
	#endif

	const char* vert_path
		= (argc >= 2 ? argv[1] : _vert_path);
	const char* frag_path
		= (argc >= 3 ? argv[2] : _frag_path);

	if (!display_init(
		"Hello md2 - GL ES 2.0",
		320, 240,
		display_renderer_gles20))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}

	GLuint program = link_shaders(vert_path, frag_path);
	if (program == 0)
	{
		fprintf(stderr, "Error: Failed to link shader program.\n");
		return EXIT_FAILURE;
	}

	/* Load texture. */
	glActiveTexture(GL_TEXTURE0);
	GLuint texture;
	{
		texture_t* tex
			= texture_import_tga(_texture_path);
		if (!tex)
		{
			fprintf(stderr, "Error: Unable to import texture.\n");
			return EXIT_FAILURE;
		}
		texture = texture_upload_es20(tex);
		texture_delete(tex);
	}
	glUniform1i(glGetUniformLocation(program, "uTexture"), 0);

	/* Load model. */
	FILE* fp = fopen(_model_path, "r");
	if (!fp)
	{
		fprintf(stderr, "Error: "
			"Unable to open model file '%s'.\n",
			_model_path);
		return EXIT_FAILURE;
	}
	md2_model_c* model
		= md2_model_import(fp);
	fclose(fp);
	if (!model)
	{
		fprintf(stderr, "Error: Unable to import model.\n");
		return EXIT_FAILURE;
	}
	
	/* GL Initialization */
	glViewport(0, 0,
		native_window_width(),
		native_window_height());
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepthf(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	
	
	trBind(program, TR_MODEL, "model");
	trBind(program, TR_VIEW, "view");
	trBind(program, TR_PROJECTION, "projection");

	lmEnable(LM_LIGHTING);
	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw(program, model);

		display_swap();
		sleep(0); /* Give up remainder of time-slice. */
	}

	return EXIT_SUCCESS;
}
