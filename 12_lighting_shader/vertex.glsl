attribute vec4 aPosition;
attribute vec2 aTexCoord;
attribute vec4 aNextPos;

const int MAX_LIGHTS = 16;

struct lmlight {
	bool enabled;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
	vec4 spot_direction;
	float spot_exponent;
	float spot_cutoff;
	float constant_attenuation;
	float linear_attenuation;
	float quadratic_attenuation;
};

struct lmmatstruct {
	vec4 emission;
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;

	float shininess;
};

struct lmstruct {
	bool enabled;
	int lightcount;
	lmmatstruct material;
	vec4 ambient;
	lmlight lights[MAX_LIGHTS];
};



uniform lmstruct lightmodel;
uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform float uTween;

varying vec2 vTexCoord;
varying vec4 lmcolour;

void main()
{
	vec4 pos = aPosition;
	int i;
	vec4 lightcolour;
	
	/* Interpolating for tweening of the model animation */
	if (uTween != 0.0)
		pos += uTween*(aNextPos - aPosition);
	
	/* Passing texture coordinates to the fragment shader */
	vTexCoord = aTexCoord;
	
	/* Calculating lighting */
	if (lightmodel.enabled) {
		lmcolour = vec4(0.0, 0.0, 0.0, 0.0);
		lmcolour += lightmodel.material.emission;
		lmcolour += lightmodel.ambient * lightmodel.material.ambient;

		for (i = 0; i < lightmodel.lightcount; i++) {

			if (lightmodel.lights[i].enabled == false) {
				lmcolour = vec4(0.0, 1.0, 0.0, 1.0);
				continue;
			}

			lightcolour = vec4(0.0, 0.0, 0.0, 0.0);
			lightcolour += lightmodel.lights[i].ambient * lightmodel.material.ambient;
			lmcolour += lightcolour;
		}
		lmcolour.a = lightmodel.material.ambient.a;
	} else {
		lmcolour = vec4(1.0, 1.0, 1.0, 1.0);
	}
	
	/* Transforming and sending vertex positions */
	gl_Position = (view * (model * pos)) * projection;
}
