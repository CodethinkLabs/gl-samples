attribute vec4 aPosition;
attribute vec2 aTexCoord;
attribute vec4 aNextPos;

uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;
uniform float uTween;

varying vec2 vTexCoord;

void main()
{
	vec4 pos = aPosition;
	if (uTween != 0.0)
		pos += uTween*(aNextPos - aPosition);
	vTexCoord = aTexCoord;
	gl_Position = (view * (model * pos)) * projection;
}
