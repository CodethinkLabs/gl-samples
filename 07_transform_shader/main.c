/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *   Jonathan Maw (jonathan.maw@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <time.h>

#include <GLES2/gl2.h>
#include "../common/display.h"
#include "../common/shader.h"
#include "../common/transform.h"
#include "../common/matrix.h"


int tr_current_matrix_mode;
GLfloat *tr_current_matrix;
GLint* tr_current_matrix_loc_p;

GLfloat tr_model[16];
GLint tr_model_loc;

GLfloat tr_view[16];
GLint tr_view_loc;

GLfloat tr_projection[16];
GLint tr_projection_loc;





GLuint link_shaders(const char* vert_path, const char* frag_path)
{
	GLuint program = glCreateProgram();
	if (program == 0)
		return 0;

	GLuint vert_shader = shader_compile_file(GL_VERTEX_SHADER, vert_path);
	glAttachShader(program, vert_shader);

	GLuint frag_shader = shader_compile_file(GL_FRAGMENT_SHADER, frag_path);
	glAttachShader(program, frag_shader);

	glBindAttribLocation(program, 0, "vPosition");

	glLinkProgram(program);

	GLint linked;
	glGetProgramiv(program, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		GLint len = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
		if (len > 1)
		{
			char* log = malloc(len * sizeof(char));
			glGetProgramInfoLog(program, len, NULL, log);
			fprintf(stderr, "Error: Failed to link shader:\n%.*s\n", len, log);
			free(log);
		}

		glDeleteProgram(program);
		return 0;
	}

	return program;
}

void draw(GLuint program)
{
	static GLfloat verts[] =
	{
		-1.0,  1.0,  1.0,
		 1.0,  1.0,  1.0,
		-1.0, -1.0,  1.0,
		 
		-1.0, -1.0,  1.0,
		 1.0, -1.0,  1.0,
		 1.0,  1.0,  1.0,
		
		-1.0,  1.0, -1.0,
		-1.0,  1.0,  1.0,
		-1.0, -1.0, -1.0,
		
		-1.0, -1.0, -1.0,
		-1.0, -1.0,  1.0,
		-1.0,  1.0,  1.0,
	};
	static int clockdivide = CLOCKS_PER_SEC / 60;
	float t = clock() / clockdivide;
	
	glUseProgram(program);

	trMatrixMode(TR_MODEL);
	trLoadIdentity();
	trScale(0.75, 0.75, 0.75);
	trRotate(t, 0.0, 1.0, 0.0);
	trSend();
	
	trMatrixMode(TR_VIEW);
	trLoadIdentity();
	trTranslate(0.0, 0.0, -2.0);
	trTranslate(0.0, 0.0, sinf(t/20.0)/2.0);
	trRotate(10, 1.0, 0.0, 0.0);
	trSend();
	
	trMatrixMode(TR_PROJECTION);
	trLoadIdentity();
	trPerspective(1, 6, 320, 240, 30);
	trSend();
	
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, verts);
	glEnableVertexAttribArray(0);

	glDrawArrays(GL_TRIANGLES, 0, 12);
	
}



int main(int argc, char* argv[])
{
	const char* _vert_path;
	const char* _frag_path;

	#if defined(__ANDROID__)
	_vert_path = "/sdcard/vertex.glsl";
	_frag_path = "/sdcard/fragment.glsl";
	#else
	_vert_path = "vertex.glsl";
	_frag_path = "fragment.glsl";
	#endif

	const char* vert_path
		= (argc >= 2 ? argv[1] : _vert_path);
	const char* frag_path
		= (argc >= 3 ? argv[2] : _frag_path);

	if (!display_init(
		"Hello MVP - GL ES 2.0",
		320, 240,
		display_renderer_gles20))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}

	GLuint program = link_shaders(vert_path, frag_path);
	if (program == 0)
	{
		fprintf(stderr, "Error: Failed to link shader program.\n");
		return EXIT_FAILURE;
	}


	/* GL Initialization */
	glViewport(0, 0, 320, 240);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
	glClearDepthf(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDepthMask(GL_TRUE);
	
	trBind(program, TR_MODEL, "model");
	trBind(program, TR_VIEW, "view");
	trBind(program, TR_PROJECTION, "projection");
	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		draw(program);

		display_swap();
		sleep(0);  // Give up remainder of time-slice.
	}

	return EXIT_SUCCESS;
}
