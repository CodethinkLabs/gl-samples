attribute vec4 vPosition;

uniform mat4 view;
uniform mat4 model;
uniform mat4 projection;

void main()
{
	vec4 pos;

	gl_Position = (view * (model * vPosition)) * projection;
}
