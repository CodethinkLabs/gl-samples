#include "buffer.h"
#include <stdlib.h>



buffer_t buffer_create(unsigned width, unsigned height, unsigned stride)
{
	if (stride == 0)
		stride = width;
	buffer_t buffer =
	{
		.width  = width,
		.height = height,
		.stride = stride,
		.data   = malloc(sizeof(pixel_t) * stride * height)
	};
	return buffer;
}

void buffer_delete(buffer_t buffer)
{
	free(buffer.data);
}



#include <GLES/gl.h>


static inline unsigned npot2(unsigned x)
{
	unsigned i;
	for (i = 1; i < x; i <<= 1);
	return i;
}

static GLint buffer_upload(buffer_t buffer, unsigned vstride)
{
	/* If not a power of 2 */
	if (buffer.stride
		& (buffer.stride - 1))
		return 0;
	if (buffer.height
		& (buffer.height - 1))
		return 0;	

	GLuint name = 0;
	glGenTextures(1, &name);
	if (!name)
		return 0;

	glBindTexture(GL_TEXTURE_2D, name);
	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RGBA,
		buffer.stride, vstride, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexSubImage2D(
		GL_TEXTURE_2D, 0,
		0, 0, buffer.stride, buffer.height,
		GL_RGBA, GL_UNSIGNED_BYTE, buffer.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	return name;
}

void buffer_render(buffer_t buffer)
{
	unsigned vstride
		= npot2(buffer.height);

	GLint name
		= buffer_upload(buffer, vstride);
	if (!name)
		return;

	float w = buffer.width
		/ (float)buffer.stride;
	float h = buffer.height
		/ (float)vstride;

	GLfloat verts[] =
	{/*        X,    Y,   Z,   U,   V,*/
		-1.0,  1.0, 0.0, 0.0, 0.0,
		 1.0,  1.0, 0.0, w  , 0.0,
		-1.0, -1.0, 0.0, 0.0, h  ,
		 1.0, -1.0, 0.0, w  , h  ,
	};

	glVertexPointer  (3, GL_FLOAT, (5 * sizeof(GLfloat)), &verts[0]);
	glTexCoordPointer(2, GL_FLOAT, (5 * sizeof(GLfloat)), &verts[3]);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_2D);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glDeleteTextures(1, &name);
}
