#ifndef __buffer_h__
#define __buffer_h__

#include "pixel.h"

typedef struct
{
	unsigned width, height, stride;
	pixel_t* data;
} buffer_t;

extern buffer_t buffer_create(unsigned width, unsigned height, unsigned stride);
extern void     buffer_delete(buffer_t buffer);

extern void buffer_render(buffer_t buffer);

#endif
