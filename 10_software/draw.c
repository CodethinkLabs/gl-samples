#include "draw.h"
#include <stdbool.h>



void draw_rect_fill(
	buffer_t buffer,
	unsigned x, unsigned y,
	unsigned w, unsigned h,
	pixel_t color)
{
	/* Culling */
	if (!buffer.data
		|| (w == 0)
		|| (h == 0)
		|| (x >= buffer.width)
		|| (y >= buffer.height))
		return;

	/* Clipping */
	if ((x + w) > buffer.width)
		w = (buffer.width - x);
	if ((y + h) > buffer.height)
		h = (buffer.height - y);

	/* Rendering */
	pixel_t* pj;
	unsigned j;
	for (j = 0, pj = &buffer.data[(y * buffer.stride) + x];
		j < h; j++, pj += buffer.stride)
	{
		pixel_t* pi;
		unsigned i;
		for (i = 0, pi = pj; i < w; i++, pi++)
			*pi = color;
	}
}

void draw_rect(
	buffer_t buffer,
	unsigned x, unsigned y,
	unsigned w, unsigned h,
	pixel_t color)
{
	/* Culling */
	if (!buffer.data
		|| (w == 0)
		|| (h == 0)
		|| (x > buffer.width)
		|| (y > buffer.height))
		return;

	/* Clipping */
	bool wclip = ((x + w) > buffer.width);
	if (wclip) w = (buffer.width - x);
	bool hclip = ((y + h) > buffer.height);
	if (hclip) h = (buffer.height - y);

	/* Rendering */
	pixel_t* pj;
	unsigned j;
	for (j = 0, pj = &buffer.data[(y * buffer.stride) + x];
		j < h; j++, pj += buffer.stride)
	{
		if ((j == 0)
			|| (!hclip && (j == (h - 1))))
		{
			pixel_t* pi;
			unsigned i;
			for (i = 0, pi = pj; i < w; i++, pi++)
				*pi = color;
		} else {
			pj[0]     = color;
			if (!wclip)
				pj[w - 1] = color;
		}
	}
}

void draw_pixel(
	buffer_t buffer,
	unsigned x, unsigned y,
	pixel_t color)
{
	if (x < buffer.width && y < buffer.height)
		buffer.data[(y * buffer.stride) + x] = color;
}

bool same_signf(float a, float b)
{
	int asgn, bsgn;
	asgn = a > 0.0 ? 1.0 : a < 0.0 ? -1.0 : 0.0;
	bsgn = b > 0.0 ? 1.0 : b < 0.0 ? -1.0 : 0.0;
	return (asgn == bsgn);
}

void draw_triangle_bound(
	buffer_t buffer,
	float *v,
	pixel_t color)
{
	unsigned i, x, y;
	/* Make coords more readable */
	float x1=v[0], x2=v[2], x3=v[4];
	float y1=v[1], y2=v[3], y3=v[5];
	/* Pre-calculate deltas */
	float dx12 = x1 - x2, dx23 = x2 - x3, dx31 = x3 - x1;
	float dy12 = y1 - y2, dy23 = y2 - y3, dy31 = y3 - y1;
	/* Calculate bounding box */
	unsigned xmin = v[0], xmax = v[0], ymin = v[1], ymax = v[1];
	for (i = 0; i < 6; i += 2) {
		if (v[i] < xmin) {
			xmin = v[i];
		} else if (v[i] > xmax) {
			xmax = v[i];
		}
	}

	for (i = 1; i < 6; i += 2) {
		if (v[i] < ymin) {
			ymin = v[i];
		} else if (v[i] > ymax) {
			ymax = v[i];
		}
	}
	/* Precalculate parts of half-edge functions */
	float c1 = dy12 * x1 - dx12 * y1;
	float c2 = dy23 * x2 - dx23 * y2;
	float c3 = dy31 * x3 - dx31 * y3;
	
	float cy1 = c1 + dx12 * ymin - dy12 * xmin;
	float cy2 = c2 + dx23 * ymin - dy23 * xmin;
	float cy3 = c3 + dx31 * ymin - dy31 * xmin;

	/* Precalculate result of half-edge for opposing verts */
	float cp1 = dx12 * dy31 - dy12 * dx31;
	float cp2 = dx23 * dy12 - dy23 * dx12;
	float cp3 = dx31 * dy23 - dy31 * dx23;
	
	for (y = ymin; y <= ymax; y++) {
		
		float cx1=cy1;
		float cx2=cy2;
		float cx3=cy3;
		for (x = xmin; x <= xmax; x++) {

			if (
				same_signf(cx1, cp1)
				&& same_signf(cx2, cp2)
				&& same_signf(cx3, cp3)
			)
				draw_pixel(buffer, x, y, color);
			
			/* Mathematically equivalent to x -> x+1 */
			cx1 -= dy12;
			cx2 -= dy23;
			cx3 -= dy31;
			
		}
	/* Mathematically equivalent to y -> y+1 */
	cy1 += dx12;
	cy2 += dx23;
	cy3 += dx31;
	}

}

static inline int mini(int a, int b, int c)
{
	return (a < b)  ?  ((a < c) ? a : c)  :  ((b < c) ? b : c);
}

static inline int maxi(int a, int b, int c)
{
	return (a > b)  ?  ((a > c) ? a : c)  :  ((b > c) ? b : c);
}

static bool same_signi(int a, int b)
{
	int asgn, bsgn;
	asgn = a > 0 ? 1 : a < 0 ? -1 : 0;
	bsgn = b > 0 ? 1 : b < 0 ? -1 : 0;
	return (asgn == bsgn);
}

void draw_triangle_bound_fixed(
	buffer_t buffer,
	float *v,
	pixel_t color)
{
	/* Coordinates converted to fixed-point */
	int x1 = (int) (16.0 * v[0]);
	int x2 = (int) (16.0 * v[2]);
	int x3 = (int) (16.0 * v[4]);

	int y1 = (int) (16.0 * v[1]);
	int y2 = (int) (16.0 * v[3]);
	int y3 = (int) (16.0 * v[5]);
	
	/* Deltas */
	int dx12 = x1 - x2;
	int dx23 = x2 - x3;
	int dx31 = x3 - x1;
	
	int dy12 = y1 - y2;
	int dy23 = y2 - y3;
	int dy31 = y3 - y1;
	
	/* Fixed-point deltas (?) */
	int fdx12 = dx12 << 4;
	int fdx23 = dx23 << 4;
	int fdx31 = dx31 << 4;
	
	int fdy12 = dy12 << 4;
	int fdy23 = dy23 << 4;
	int fdy31 = dy31 << 4;
	
	/* Bounding rectangle */
	int xmin = (mini(x1, x2, x3) + 0xF) >> 4;
	int xmax = (maxi(x1, x2, x3) + 0xF) >> 4;
	int ymin = (mini(y1, y2, y3) + 0xF) >> 4;
	int ymax = (maxi(y1, y2, y3) + 0xF) >> 4;
	
	/* Half-edge constants */
	int c1 = dy12 * x1 - dx12 * y1;
	int c2 = dy23 * x2 - dx23 * y2;
	int c3 = dy31 * x3 - dx31 * y3;
	
	/* Correct for fill convention */
	if (dy12 < 0 || (dy12 == 0 && dx12 > 0))
		c1++;
	if (dy23 < 0 || (dy23 == 0 && dx23 > 0))
		c2++;
	if (dy31 < 0 || (dy31 == 0 && dx31 > 0))
		c3++;
	
	int cy1 = c1 + dx12 * (ymin << 4) - dy12 * (xmin << 4);
	int cy2 = c2 + dx23 * (ymin << 4) - dy23 * (xmin << 4);
	int cy3 = c3 + dx31 * (ymin << 4) - dy31 * (xmin << 4);
	
	/* Precalculate result of half-edge for opposing verts */
	int cp1 = dx12 * dy31 - dy12 * dx31;
	int cp2 = dx23 * dy12 - dy23 * dx12;
	int cp3 = dx31 * dy23 - dy31 * dx23;
	
	unsigned x, y;
	for (y = ymin; y < ymax; y++) {
		int cx1 = cy1;
		int cx2 = cy2;
		int cx3 = cy3;
		
		for (x = xmin; x < xmax; x++)
		{
			if(
				same_signi(cx1, cp1)
				&& same_signi(cx2, cp2)
				&& same_signi(cx3, cp3)
			)
				draw_pixel(buffer, x, y, color);
			
			cx1 -= fdy12;
			cx2 -= fdy23;
			cx3 -= fdy31;
		}
		
		cy1 += fdx12;
		cy2 += fdx23;
		cy3 += fdx31;
	}
}

void draw_triangle_bound_fixed_tiled(
	buffer_t buffer,
	float *v,
	pixel_t color)
{
	/* Coordinates converted to fixed-point */
	int x1 = (int) (16.0 * v[0]);
	int x2 = (int) (16.0 * v[2]);
	int x3 = (int) (16.0 * v[4]);

	int y1 = (int) (16.0 * v[1]);
	int y2 = (int) (16.0 * v[3]);
	int y3 = (int) (16.0 * v[5]);
	
	/* Deltas */
	int dx12 = x1 - x2;
	int dx23 = x2 - x3;
	int dx31 = x3 - x1;
	
	int dy12 = y1 - y2;
	int dy23 = y2 - y3;
	int dy31 = y3 - y1;
	
	/* Fixed-point deltas (?) */
	int fdx12 = dx12 << 4;
	int fdx23 = dx23 << 4;
	int fdx31 = dx31 << 4;
	
	int fdy12 = dy12 << 4;
	int fdy23 = dy23 << 4;
	int fdy31 = dy31 << 4;
	
	
	/* Bounding rectangle */
	int xmin = (mini(x1, x2, x3) + 0xF) >> 4;
	int xmax = (maxi(x1, x2, x3) + 0xF) >> 4;
	int ymin = (mini(y1, y2, y3) + 0xF) >> 4;
	int ymax = (maxi(y1, y2, y3) + 0xF) >> 4;

	/* Tile size*/
	int ts = 8;
	
	/* Round x,y min to nearest tile */
	xmin &= ~(ts - 1);
	ymin &= ~(ts - 1);
	
	/* Half-edge constants */
	int c1 = dy12 * x1 - dx12 * y1;
	int c2 = dy23 * x2 - dx23 * y2;
	int c3 = dy31 * x3 - dx31 * y3;
	
	/* Correct for fill convention */
	if (dy12 < 0 || (dy12 == 0 && dx12 > 0))
		c1++;
	if (dy23 < 0 || (dy23 == 0 && dx23 > 0))
		c2++;
	if (dy31 < 0 || (dy31 == 0 && dx31 > 0))
		c3++;
	
	int cy1 = c1 + dx12 * (ymin << 4) - dy12 * (xmin << 4);
	int cy2 = c2 + dx23 * (ymin << 4) - dy23 * (xmin << 4);
	int cy3 = c3 + dx31 * (ymin << 4) - dy31 * (xmin << 4);
	
	/* Precalculate result of half-edge for opposing verts */
	int cp1 = dx12 * dy31 - dy12 * dx31;
	int cp2 = dx23 * dy12 - dy23 * dx12;
	int cp3 = dx31 * dy23 - dy31 * dx23;
	
	for (int y = ymin; y < ymax; y += ts) {
		for (int x = xmin; x < xmax; x += ts) {
			/* Get corners of block */
			int x0 = x << 4;
			int y0 = y << 4;
			int x1 = (x + ts - 1) << 4;
			int y1 = (y + ts - 1) << 4;
			
			/* Evaluate half-space functions for every corner */
			/* Edge 12 */
			bool a00 = same_signi(c1 + dx12 * y0 - dy12 * x0, cp1);
			bool a10 = same_signi(c1 + dx12 * y0 - dy12 * x1, cp1);
			bool a01 = same_signi(c1 + dx12 * y1 - dy12 * x0, cp1);
			bool a11 = same_signi(c1 + dx12 * y1 - dy12 * x1, cp1);
			uint8_t a = (a00 << 0) | (a10 << 1) | (a01 << 2) | (a11 << 3);		

			/* Edge 23 */
			bool b00 = same_signi(c2 + dx23 * y0 - dy23 * x0, cp2);
			bool b10 = same_signi(c2 + dx23 * y0 - dy23 * x1, cp2);
			bool b01 = same_signi(c2 + dx23 * y1 - dy23 * x0, cp2);
			bool b11 = same_signi(c2 + dx23 * y1 - dy23 * x1, cp2);
			uint8_t b = (b00 << 0) | (b10 << 1) | (b01 << 2) | (b11 << 3);		

			/* Edge 31 */
			bool c00 = same_signi(c3 + dx31 * y0 - dy31 * x0, cp3);
			bool c10 = same_signi(c3 + dx31 * y0 - dy31 * x1, cp3);
			bool c01 = same_signi(c3 + dx31 * y1 - dy31 * x0, cp3);
			bool c11 = same_signi(c3 + dx31 * y1 - dy31 * x1, cp3);
			uint8_t c = (c00 << 0) | (c10 << 1) | (c01 << 2) | (c11 << 3);		

			/* Do nothing if none of the triangle is in the tile */
			if (a == 0 || b == 0 || c == 0)
				continue;
			/* Fill the tile in if it's fully covered by the triangle */
			if (a == 0xF && b == 0xF && c == 0xF) {
				for(int iy = y; iy < y + ts; iy++) {
					for(int ix = x; ix < x + ts; ix++) {
						draw_pixel(buffer, ix, iy, color);
					}
				}
			} else {
				int cy1 = c1 + dx12 * y0 - dy12 * x0;
				int cy2 = c2 + dx23 * y0 - dy23 * x0;
				int cy3 = c3 + dx31 * y0 - dy31 * x0;
				
				for(int iy = y; iy < y + ts; iy++) {
					int cx1 = cy1;
					int cx2 = cy2;
					int cx3 = cy3;
					
					for(int ix = x; ix < x + ts; ix++) {
						if (
							same_signi(cx1, cp1)
							&& same_signi(cx2, cp2)
							&& same_signi(cx3, cp3)
						)
							draw_pixel(buffer, ix, iy, color);
						cx1 -= fdy12;
						cx2 -= fdy23;
						cx3 -= fdy31;
					}
				cy1 += fdx12;
				cy2 += fdx23;
				cy3 += fdx31;
				}
			}
		}
	}
}
