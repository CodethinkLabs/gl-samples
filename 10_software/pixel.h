#ifndef __pixel_h__
#define __pixel_h__

#include <stdint.h>



typedef struct
__attribute__((__packed__))
{
	uint8_t r, g, b, a;
} pixel_t;

static inline const pixel_t pixel(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
	{ return (pixel_t){ r, g, b, a }; }

static inline const uint8_t clampf_u8(float x)
{
	if (x <  0.0f) return 0x00;
	if (x >= 1.0f) return 0xFF;
	return (x * 0xFF);
}

static inline const pixel_t pixelf(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	return pixel(
		clampf_u8(r), clampf_u8(g),
		clampf_u8(b), clampf_u8(a));
}

#endif
