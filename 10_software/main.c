/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>

#include <GLES/gl.h>
#include "../common/display.h"

#include "buffer.h"
#include "draw.h"



int main(int argc, char* argv[])
{
	srand(time(NULL));
	if (!display_init(
		"Hello Software Rendering",
		320, 240,
		display_renderer_gles1x))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}
		
	/* GL Initialization */
	glViewport(0, 0, 320, 240);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	/* SW Buffer Initialization */
	buffer_t buffer
		= buffer_create(320, 256, 512);
	float verts[] = {
		rand() % 320, rand() % 240,
		rand() % 320, rand() % 240,
		rand() % 320, rand() % 240,
	};

	draw_rect_fill(
		buffer,
		0, 0, 320, 240,
		pixelf(0.0, 0.0, 0.0, 1.0));
	clock_t oldclock = clock();
	for (unsigned i = 0; i < 1000; i++) {
		draw_triangle_bound_fixed_tiled(
			buffer,
			verts,
			pixelf(1.0, 1.0, 1.0, 1.0));
	}
	printf("It took %f seconds to draw 1000 fixed-point tiled bound triangles\n", 
		(double)(clock() - oldclock) / CLOCKS_PER_SEC);

	draw_pixel(
		buffer,
		verts[0], verts[1],
		pixelf(1.0, 0.0, 0.0, 1.0));
	draw_pixel(
		buffer,
		verts[2], verts[3],
		pixelf(0.0, 1.0, 0.0, 1.0));
	draw_pixel(
		buffer,
		verts[4], verts[5],
		pixelf(0.0, 0.0, 1.0, 1.0));

	buffer_render(buffer);
	display_swap();
	while (true)
	{
		sleep(0);  // Give up remainder of time-slice.
	}

	return EXIT_SUCCESS;
}
