#ifndef __draw_h__
#define __draw_h__

#include "buffer.h"

extern void draw_rect_fill(
	buffer_t buffer,
	unsigned x, unsigned y,
	unsigned w, unsigned h,
	pixel_t color);

extern void draw_rect(
	buffer_t buffer,
	unsigned x, unsigned y,
	unsigned w, unsigned h,
	pixel_t color);

extern void draw_pixel(
	buffer_t buffer,
	unsigned x, unsigned y,
	pixel_t color);

extern void draw_triangle_bound(
	buffer_t buffer,
	float *v,
	pixel_t color);

extern void draw_triangle_bound_fixed(
	buffer_t buffer,
	float *v,
	pixel_t color);

extern void draw_triangle_bound_fixed_tiled(
	buffer_t buffer,
	float *v,
	pixel_t color);


#endif
