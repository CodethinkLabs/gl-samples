/* Author(s):
 *   Ben Brewer (ben.brewer@codethink.co.uk)
 *
 * Copyright (c) 2012 Codethink (http://www.codethink.co.uk)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */



#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>

#include "../common/display.h"
#include <GLES/gl.h>



int main(int argc, char* argv[])
{
	if (!display_init(
		"Hello Display - GL ES 1.x",
		320, 240,
		display_renderer_gles20))
	{
		fprintf(stderr, "Error: Unable to open display.\n");
		return EXIT_FAILURE;
	}

	/* GL Initialization */
	glClearColor(1.0f, 0.0f, 0.0f, 0.0f);
	/* TODO - GL initialization calls here */

	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT);

		/* TODO - GL Drawing calls here */

		display_swap();

		sleep(0);  // Give up remainder of time-slice.
	}

	return EXIT_SUCCESS;
}
